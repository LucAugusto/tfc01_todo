package com.example.todo.activities.data

object DBSSchema {
    object TaskTable {
        const val TABLENAME = "tasks"
        const val ID = "c_id"
        const val TXTTASK = "c_task"
        const val URGENCIA = "c_urgencia"
        fun getCreateTableQuery(): String {
            return """
                 CREATE TABLE IF NOT EXISTS $TABLENAME (
                    $ID INTEGER PRIMARY KEY AUTOINCREMENT,
                    $TXTTASK TEXT NOT NULL,
                    $URGENCIA BOOLEAN
                 );
            """.trimIndent()
        }
    }

}