package com.example.todo.activities.lists.lists

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.activities.classes.Tarefa
import java.util.ArrayList

class TasksListAdapter(val c:Context,val taskList:ArrayList<Tarefa>):
    RecyclerView.Adapter<TasksListAdapter.TasksListViewHolder>() {

    inner class TasksListViewHolder(val v: View) : RecyclerView.ViewHolder(v){

        val txtTask = v.findViewById<TextView>(R.id.txt_Task)
        val feito = v.findViewById<CheckBox>(R.id.cb_feito)
        val urgencia = v.findViewById<Switch>(R.id.sw_importancia)
        val cor_urgencia = v.findViewById<LinearLayout>(R.id.lnl_cor)
    }

    private var listener: OnClickTaskListener? = null
    private var selectListener: OnSelectTaskListener? = null

    interface OnClickTaskListener {
        fun onClickTask(tarefa: Tarefa)
    }
    interface OnSelectTaskListener {
        fun onSelectTask(tarefa: Tarefa, isSelected: Boolean)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TasksListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
            val v = inflater.inflate(R.layout.view_task,parent,false)
            return TasksListViewHolder(v)
    }

    override fun onBindViewHolder(holder: TasksListViewHolder, position: Int) {
        val newTask = taskList[position]
            holder.txtTask.text = newTask.texto
            holder.feito.setChecked(false)
            if(holder.urgencia.isChecked){
                newTask.tipo = true
                holder.cor_urgencia.setBackgroundColor(Color.RED)
            }else{
                newTask.tipo = false
                holder.cor_urgencia.setBackgroundColor(Color.GREEN)
            }

    }

    override fun getItemCount(): Int {
        return taskList.size
    }

    fun setOnClickTaskListener(listener: OnClickTaskListener?): TasksListAdapter {
        this.listener = listener
        return this
    }
    fun getOnClickTaskListener(): OnClickTaskListener? {
        return this.listener
    }
    fun setOnSelectChannelListener(selectListener: OnSelectTaskListener?): TasksListAdapter {
        this.selectListener = selectListener
        return this
    }
    fun getOnSelectChannelListener(): OnSelectTaskListener? {
        return this.selectListener
    }
}