package com.example.todo.activities.data

import android.content.ContentValues
import android.content.Context
import com.example.todo.activities.classes.Tarefa

class DBTaskTable(context: Context) {
    companion object {
        private const val CH_WHERECLAUSE_ID = "${DBSSchema.TaskTable.ID} = ?"
        private const val CH_ORDERBY = "${DBSSchema.TaskTable.ID} DESC"
        private val CH_COLS = arrayOf(DBSSchema.TaskTable.ID, DBSSchema.TaskTable.TXTTASK, DBSSchema.TaskTable.URGENCIA)
    }
    private val dbHelper = DBHelper(context)
    // INSERT INTO channels (c_name) VALUES ("Familia");
    fun insert(task: Tarefa): Int {
        val cv = ContentValues()
        cv.put(DBSSchema.TaskTable.TXTTASK, task.getName())
        val db = this.dbHelper.writableDatabase
        val id = db.insert(DBSSchema.TaskTable.TABLENAME, null, cv)
        db.close()
        return id.toInt()
    }
    // =========================================
    fun delete(channel: Tarefa) {
        val db = this.dbHelper.writableDatabase
        db.delete(DBSSchema.TaskTable.TABLENAME, CH_WHERECLAUSE_ID, arrayOf(channel.getId().toString()))
        db.close()
    }
    // =========================================
    fun updateTaskText(task: Tarefa) {
        val cv = ContentValues()
        cv.put(DBSSchema.TaskTable.TXTTASK, task.getName())
        val db = this.dbHelper.writableDatabase
        db.update(DBSSchema.TaskTable.TABLENAME, cv, CH_WHERECLAUSE_ID, arrayOf(task.getId().toString()))
        db.close()
    }
    // =========================================
    fun getAllTasks(): ArrayList<Tarefa> {
        val tasks = ArrayList<Tarefa>()
        val db = this.dbHelper.readableDatabase
        val cur = db.query(DBSSchema.TaskTable.TABLENAME,
            CH_COLS,
            null,
            null,
            null,
            null,
            CH_ORDERBY)
        while(cur.moveToNext()) {
            val id = cur.getInt(cur.getColumnIndex(DBSSchema.TaskTable.ID))
            val txt = cur.getString(cur.getColumnIndex(DBSSchema.TaskTable.TXTTASK))
            val task = Tarefa(txt, false, false, id)
            tasks.add(task)
        }
        cur.close()
        db.close()
        return tasks
    }
}
