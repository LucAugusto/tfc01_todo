package com.example.todo.activities.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.activities.classes.Tarefa
import com.example.todo.activities.data.TasksDAOSingleton
import com.example.todo.activities.lists.lists.TasksListAdapter


class MainActivity : AppCompatActivity() {
    var tarefasList: ArrayList<Tarefa> = TasksDAOSingleton.getTasks()
    private lateinit var recyclerView: RecyclerView
    lateinit var adapter : TasksListAdapter
    private lateinit var taskListAdapter: TasksListAdapter
    private lateinit var okButton:Button

//   private lateinit var resultLauncher: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tarefasList = ArrayList()
        okButton = findViewById(R.id.btnOK)
        recyclerView = findViewById(R.id.rv_tasks)

        adapter = TasksListAdapter(this, tarefasList)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        okButton.setOnClickListener{addTask()}



//        val UW: CheckBox = findViewById(R.id.cb_UW)
//
//        UW.setOnCheckedChangeListener{buttonView, isChecked ->
//            if(isChecked){
//                this.tarefasList.renderDone()
//            }else{
//                this.tarefasList.render()
//            }
//
//        }


    }

    private fun addTask() {
        val inflter = LayoutInflater.from(this)
        val v = inflter.inflate(R.layout.view_task, null)
        val txtTask = v.findViewById<TextView>(R.id.txt_Task)
        var urg = v.findViewById<Switch>(R.id.sw_importancia)

        val tasktxt = txtTask.text.toString()
        var imp: Boolean = false
//        if(urg.isChecked){
//            imp = true
//        }else{
//            imp = false
//        }

        tarefasList.add(Tarefa(tasktxt, imp,false,0))
        Toast.makeText(this,"Deu certo, gata!",Toast.LENGTH_SHORT).show()
    }

//    fun onClickDeleteTask(v: View) {
//
//        TasksListAdapter.selectedTasks.forEach { _, value ->
//            val pos = ChannelDAOSingleton.delete(baseContext, value)
//            channelListAdapter.notifyItemRemoved(pos)
//                }
//        channelListAdapter.selectedChannels.clear()
//        fabDeleteChannel.visibility = View.GONE
//        fabEditChannel.visibility = View.GONE
//
//
//    }



}

