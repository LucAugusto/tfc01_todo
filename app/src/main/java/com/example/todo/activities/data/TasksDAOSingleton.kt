package com.example.todo.activities.data

import com.example.todo.activities.classes.Tarefa

object TasksDAOSingleton {
    private var tasksList: ArrayList<Tarefa> = ArrayList()
    private var serial: Int = 0
    init {
        for(task in this.tasksList) {
            task.setId(this.serial++)
        }
    }
    fun add(tarefa: Tarefa) {
        this.tasksList.add(tarefa)
    }
    fun getTasks(): ArrayList<Tarefa> {
        return this.tasksList
    }

}