package com.example.todo.activities.classes

import android.os.Parcel
import android.os.Parcelable

class Tarefa {

    var texto: String;
    var tipo: Boolean;
    var feita: Boolean = false;
    private var id: Int = 0

    constructor(descricao: String, importancia: Boolean, feita: Boolean, id: Int){
        this.texto = descricao;
        this.tipo = importancia;
        this.feita = feita;
        this.id = id;
    }

    fun getId(): Int {
        return this.id
    }
    fun setId(id: Int) {
        this.id = id
    }
    fun getName(): String {
        return this.texto
    }
    fun getType(): Boolean {
        if (this.feita == true){
            return true
        }else{
            return false
        }
    }
//==================================

}