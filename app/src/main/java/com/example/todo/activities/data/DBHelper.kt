package com.example.todo.activities.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(
               context: Context
): SQLiteOpenHelper(context, DBNAME, null, DBVERSION) {
    companion object {
        private const val DBNAME = "pdm_messenger.db"
        private const val DBVERSION = 1
    }

    override fun onOpen(db: SQLiteDatabase?) {
        super.onOpen(db)
        db?.execSQL("PRAGMA foreign_keys=ON")
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(DBSSchema.TaskTable.getCreateTableQuery())
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        // "Not implemented"
    }
}
